import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Search = ({ searchUsers, showClear, clearUsers, setAlert }) => {
  const [text, setText] = useState('');

  const onChange = (e) => {
    setText(e.target.value);
  };

  const onSubmit = (e) => {
    e.preventDefault();

    //if empty search is done
    if (text === '') {
      setAlert('Enter some name', 'light');
    } else {
      console.log(text);
      searchUsers(text);
      setText('');
    }
  };

  return (
    <div>
      <form onSubmit={onSubmit} className='form-inline'>
        <input
          type='text'
          className='form-control mr-sm-2 mt-3'
          value={text}
          onChange={onChange}
          name='text'
          placeholder='Search User..'
        />

        <input type='submit' value='Search' className='btn btn-success mt-3' />
      </form>
      {showClear && (
        <button className='btn btn-danger mt-3 mb-2' onClick={clearUsers}>
          Clear
        </button>
      )}
    </div>
  );
};

Search.propTypes = {
  //ptfr shortcut
  searchUsers: PropTypes.func.isRequired,
  clearUsers: PropTypes.func.isRequired,
  //ptbr shortcut
  showClear: PropTypes.bool.isRequired,
  setAlert: PropTypes.func.isRequired,
};

export default Search;
