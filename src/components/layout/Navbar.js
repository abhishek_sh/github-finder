import React from 'react';
import PropTypes from 'prop-types';
// link for state to be intact after search
import { Link } from 'react-router-dom';
const Navbar = (props) => {
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-info'>
      <h1>
        <i className='fab fa-github' />
        {props.title}
      </h1>
      <ul className='navbar-nav ml-auto'>
        <li>
          <Link to='/' className='nav-link'>
            <h5 className='text-dark'>Home</h5>
          </Link>
        </li>
        <li>
          <Link to='/about' className='nav-link'>
            <h5 className='text-dark'>About</h5>
          </Link>
        </li>
      </ul>
    </nav>
  );
};

//if props not passed
Navbar.defaultProps = {
  title: 'Git Searcher Hub',
};

//define data type of props
Navbar.propTypes = {
  title: PropTypes.string.isRequired, //ptsr
};

export default Navbar;
