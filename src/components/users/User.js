import React, { Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import Spinner from '../layout/Spinner';
import { Link } from 'react-router-dom';
import Repos from '../repos/Repos';

const User = ({ user, loading, getUser, getUserRepos, repos, match }) => {
  // componentDidMount() {
  //   this.props.getUser(this.props.match.params.login);
  //   this.props.getUserRepos(this.props.match.params.login);
  // }

  //componentDidMount  -->> useEffect hook
  useEffect(() => {
    getUser(match.params.login);
    getUserRepos(match.params.login);
    //eslint-disable-next-line
  }, []); // [] to stop request(i.e only once run it).for eg, if [repos] we want to run when repos change

  const {
    name,
    avatar_url,
    location,
    bio,
    blog,
    login,
    html_url,
    followers,
    following,
    public_repos,
    public_gists,
    hireable,
  } = user;

  if (loading) return <Spinner />;

  return (
    <Fragment>
      <Link to='/' className='btn btn-primary mt-3 mr-2'>
        Back to Search
      </Link>
      <h6 className='mt-2 text-center '>
        Hireable: {''}
        {hireable ? (
          <i className='fa fa-check' aria-hidden='true'></i>
        ) : (
          <i className='fas fa-times-circle text-danger' aria-hidden='true'></i>
        )}
      </h6>
      <div className='card-deck mt-2'>
        <div className='card text-center'>
          <img
            src={avatar_url}
            className='rounded-circle mx-auto d-block card-img-top'
            style={{ width: '150px' }}
            alt=''
          />
          <div className='card-body'>
            <h1>{name}</h1>
            <p>Location: {location}</p>

            {bio && (
              <Fragment>
                <h3>Bio:</h3>
                <p>{bio}</p>
              </Fragment>
            )}
            <a href={html_url} className='btn btn-warning mb-2'>
              Github Profile
            </a>

            <h5>
              <u>Username: </u>
              {login}{' '}
            </h5>
            <h5>
              <u>Blog:</u>
              {blog}{' '}
            </h5>
          </div>
          <div className='card-footer'>
            <div className='badge badge-success mr-3'>
              Followers: {followers}
            </div>
            <div className='badge badge-secondary mr-3'>
              Following: {following}
            </div>
            <div className='badge badge-danger mr-3'>
              Public repositories: {public_repos}
            </div>
            <div className='badge badge-dark mr-3'>
              Public gists: {public_gists}
            </div>
          </div>
        </div>
      </div>

      <Repos repos={repos} />
    </Fragment>
  );
};

User.propTypes = {
  loading: PropTypes.bool, //ptb
  user: PropTypes.object.isRequired, //ptor
  repos: PropTypes.array.isRequired,
  getUser: PropTypes.func.isRequired, //ptfr
  getUserRepos: PropTypes.func.isRequired,
};

export default User;
